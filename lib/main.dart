import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'Pages/home.dart';


void main(){

runApp(Tasks());
}


class Tasks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tasks',
      home:  Home(),
      theme: ThemeData(
        primarySwatch: Colors.grey,
        accentColor: Colors.teal[100],
      ),
    );
  }
}
