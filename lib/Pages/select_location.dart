import 'package:flutter/material.dart';
import 'package:tasks/services/get_store_locations.dart';
import 'package:tasks/widgets/dropdown_button.dart';

class SelectLocation extends StatefulWidget {
  final List data;

  SelectLocation({this.data});

  @override
  _SelectLocationState createState() => _SelectLocationState();
}

class _SelectLocationState extends State<SelectLocation> {
  String accessToken;
  String tokenType;

  String storeLocationUrl =
      'https://storeak-gps-service-beta.azurewebsites.net/api/v1/Locations';

  // _countriesData variable will store all data from previous URL
  List _countriesData = [];

  // _citiesData variable will store all cities data for specific city.
  List _citiesData = [];

  // _locations variable will store all countries names from _countriesData list.
  // we will show this names in the Countries DropDownButton
  List<String> _locations = [];

  // _citiesNames variable will store all cities names for specific country.
  // we will show this names in the cities DropDownButton
  List<String> _citiesNames = [];

  // _districtsNames variable will store all districts names for specific city.
  // we will show this names in the districts DropDownButton
  List<String> _districtsNames = [];

  // _selectedCountry variable will store the country that a user select it from countries DropDownButton.
  String _selectedCountry;

  // _selectedCity variable will store the city that a user select it from cities DropDownButton.
  String _selectedCity;

  // _selectedDistrict variable will store the district that a user select it from districts DropDownButton.
  String _selectedDistrict;

  @override
  initState() {
    super.initState();
    // To initialize variables with correct value
    accessToken = widget.data[0]['access_token'];
    tokenType = widget.data[0]['token_type'];
    // NOTE: we did that inside initState() so we can access the data coming from previous page (home class).
  }

  // This method we will use it when user click on the 'done' icon in our appBar.
  goBackToStoreInfoPage({context}) {
    Navigator.of(context).pop();
  }

  // this method will be invoked when user change country from countries DropDownButton
  // the first variable ('data'): represents the data which we will fetch from it
  // all cities names for the current country name.
  // the second variable ('countryName') : represents the current country name.
  void _onSelectedCountry(List data, String countryName) {
    setState(() {
      // when user change the selected country then both the city name and district name will be
      // changed too, cause he wants to select a new city and district.
      // so we but both of them null values as following:
      _selectedCity = null;
      _selectedDistrict = null;

      // Here we change the selected country to the new value.
      _selectedCountry = countryName;

      // Here we fetch the new cities names for the new country name.
      _citiesNames = getCitiesNames(data, countryName);

      // Here we put districts names list as empty cause the country has changed.
      _districtsNames = [];
    });
  }

  // this method will be invoked when user change city from cities DropDownButton
  // the first variable ('data'): represents the data which we will fetch from it
  // all districts names for current city name.
  // the second variable ('cityName') : represents the current city name.
  void _onSelectedCity(List data, String cityName) {
    setState(() {
      // when user change the selected city then district name will be
      // changed too, cause he wants to select a new district.
      _selectedDistrict = null;

      // Here we change the selected city to the new value.
      _selectedCity = cityName;

      // Here we fetch cities data for specific country.
      _citiesData = getCitiesData(_countriesData, _selectedCountry);

      // Here we fetch new districts names for the new city name.
      _districtsNames = getDistrictsNames(_citiesData, cityName);
    });
  }

  // this method will be invoked when user change district from districts DropDownButton
  // the variable ('districtName') : represents the current district name.
  void _onSelectedDistrict(String districtName) {
    setState(() {
      _selectedDistrict = districtName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Select Location'),
          backgroundColor: Colors.grey,
          actions: [
            IconButton(
              icon: Icon(Icons.done),
              iconSize: 35.0,
              color: Colors.teal[100],
              onPressed: () => goBackToStoreInfoPage(context: context),
            ),
          ],
        ),
        body: FutureBuilder(
          // the following method coming from get_store_locations file in the services package,
          // this method returns locations of stores, and we pass 3 parameters for http get request.
          future: getStoreLocation(storeLocationUrl, accessToken, tokenType),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }
            _countriesData = snapshot.data as List;
            _locations = getCountriesNames(_countriesData);
            return Row(

              textDirection: TextDirection.rtl,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // DropDownButtonWidget is a DropDownButton Widget
                // it is in file: widgets/dropdown_button.dart
                DropDownButtonWidget(
                  namesList: _locations,
                  selectedValue: _selectedCountry,
                  onChanged: (value) =>
                      _onSelectedCountry(snapshot.data, value),
                  hint: 'الدولة',
                ),
                DropDownButtonWidget(
                  namesList: _citiesNames,
                  selectedValue: _selectedCity,
                  onChanged: (value) =>
                      _onSelectedCity(_citiesNames, value),
                  hint: 'المحافظة',
                ),
                DropDownButtonWidget(
                  namesList: _districtsNames,
                  selectedValue: _selectedDistrict,
                  onChanged: (value) => _onSelectedDistrict(value),
                  hint: 'المنطقة',
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
