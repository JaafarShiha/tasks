import 'package:flutter/material.dart';
import 'package:tasks/Pages/select_location.dart';
import 'package:tasks/models/store.dart';
import 'package:tasks/services/get_store_info.dart';
import 'package:tasks/widgets/store_info_card.dart';

class StoreInfo extends StatefulWidget {
  // This List contain the data coming from API through Home class.
  final List data;

  StoreInfo({this.data});

  @override
  _StoreInfoState createState() => _StoreInfoState();
}

class _StoreInfoState extends State<StoreInfo> {

  // These tow variables will store the access token and token type from http.post
  String accessToken;
  String tokenType;

  String storeInfoUrl =
      'https://storeak-stores-service-beta.azurewebsites.net/api/v1/Stores/Info/StoreAndBranchesOrderedByAddresses';

  @override
  initState() {
    super.initState();
    // To initialize variables with correct value
    accessToken = widget.data[0]['access_token'];
    tokenType = widget.data[0]['token_type'];
    // NOTE: we did that inside initState() so we can access the data coming from previous page (home class).
  }

  // Building the first tab: main store info tab.
  buildMainStoreTab(Store store) {

    // StoreInfoCard is a Widget in widgets file
    return StoreInfoCard(
      image: store.mainStore.originalLogoPath,
      title: store.mainStore.name,
      description: store.mainStore.description,
      phoneNumber: store.mainStore.phoneNumber1,
      // the explanation of this variable functionality is in StoreInfoCard class.
      // we want the main store info to be expanded
      isSelected: true,
    );
  }

  // This method returns all store branches.
  getBranches(Store store) {

    var countriesBranches = store.countriesBranches;

    // in this List we will store 'StoreInfoCard' widgets and then display it in the screen.
    var cards = List<Widget>();

    for (int i = 0; i < countriesBranches.length; i++) {

      // for each country get all cities branches.
      var citiesBranches = store.countriesBranches[i].citiesBranches;

      for (int j = 0; j < citiesBranches.length; j++) {

        // for each city in the country get all store branches.
        var branches = store.countriesBranches[i].citiesBranches[j].branches;

        for (int k = 0; k < branches.length; k++) {
          // for each store branch, create a 'StoreInfoCard' widget
          cards.add(StoreInfoCard(
            image: branches[k].originalLogoPath,
            title: branches[k].name,
            description: branches[k].description,
            phoneNumber: branches[k].phoneNumber1,
            isSelected: false,
          ));
        }
      }
    }
    // returning these widgets to branches tab to display it.
    return cards;
  }

  // This method builds the Branches tab.
  buildOtherStoresTab(Store store) {
    return ListView(
      children: [
        Column(
          children: getBranches(store),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            bottom: TabBar(
              tabs: [
                Tab(
                  text: 'Main Store',
                ),
                Tab(
                  text: 'Branches',
                ),
              ],
            ),
            title: Text('Store Information'),
            actions: [
              IconButton(
                icon: Icon(Icons.location_on),
                iconSize: 35.0,
                color: Theme.of(context).accentColor,
                tooltip: 'Select your location',
                onPressed: () async {
                  Navigator.push(
                    context,
                    // SelectLocation is the third page which will contain the drop down buttons.
                    MaterialPageRoute(
                      builder: (context) => SelectLocation(
                        data: widget.data,
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
          body: FutureBuilder(
              // the following method coming from get_store_info file in the services package,
              // this method return the data of store, and we pass 3 parameters for http get request.
              future: getStoreInfo(storeInfoUrl, accessToken, tokenType),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(child: CircularProgressIndicator());
                }
                // This instance of Store object will represent the store information coming back from http.get request
                Store store = Store.fromJson(snapshot.data);

                return TabBarView(
                  children: [
                    // this function to build the first tab body (Main store body)
                    buildMainStoreTab(store),
                    // this function to build the second tab body (branches stores body)
                    buildOtherStoresTab(store),
                  ],
                );
              }),
        ),
      ),
    );
  }
}
