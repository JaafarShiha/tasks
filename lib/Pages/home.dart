import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:tasks/Pages/store_info.dart';
import 'package:tasks/models/token.dart';
import 'package:tasks/services/get_token.dart';
import 'package:tasks/utilities/constants.dart';
import 'package:tasks/widgets/dialog.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  _HomeState();

  // This key to fetch the state of the username Filed (if it contained less than 3 characters or not).
  final _formKey = GlobalKey<FormState>();

  // In this List we will put the data received from http post request and pass it to the next page(StorInfo).
  List data = [];

  // The Following parameters we will use it as a body to http post request.

  String username;
  String password;
  String playerId;
  String language;
  int gmt;
  bool showSpinner = false;

  // This variable will get the user's language to store it in language variable
  Locale myLocale;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // Here i will get the UTC time zone to store it in gmt variable which will be passed to http post request:

    // 1)- First, getting the time offset:
    Duration duration = DateTime.now()
        .timeZoneOffset; // This will return the following value 2:00:00.000000 ( in Syria)

    // 2)- Then, converting this to String to be able to cut out the first character of it:
    String durationString = duration.toString().substring(0, 1);

    // 3)- Finally, converting the string to int to store it in gmt variable:
    gmt = int.parse(durationString);
  }

  Token getTokenBody() {
    Token token = Token();

    token.playerId = "";
    token.language = myLocale.toString().substring(0, 2);
    token.gmt = gmt;
    token.isFromNotification = false;

    // these two values i put them in separate file cause they are constants and secret data.
    // file name: utilities/constants
    token.clientId = clientId;
    token.clientSecret = clientSecret;

    return token;
  }

  enterStoreInfoPage({Response response}) {
    // getting the result from http post request.
    String body = response.body;

    // storing the result in a list after decoding it
    setState(() {
      data.add(jsonDecode(body));
    });

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => StoreInfo(data: data),
      ),
    );
  }

  buildErrorDialog({Response response}) {
    // this variable to change the string result to json form
    // and from here we will get the error message that will be shown in the error dialog to the user.
    final body = json.decode(response.body);

    // Show a dialog with the error message
    showDialog(
      context: context,
      builder: (BuildContext context) => errorDialog(
        context,
        body['message'],
      ),
    );
  }

  // This method will be called when the user press the Login button
  getTokenforUser({bool isLoggedUser}) async {

    // Build token body
    Token tokenBody = getTokenBody();
    // if user is logged user add username and password to the body of request.
    if (isLoggedUser) {
      tokenBody.username = username;
      tokenBody.password = password;
    }

     Response response = isLoggedUser
        ? await getToken(requestBody: tokenBody.toJson())
        : await getAnonymousToken(requestBody: tokenBody.toJson());

    return response;
  }

  // this method will check if the username is accepted or not.
  bool checkString(String value) {
    if (value.trim().length < 3 || value.isEmpty) return false;
    return true;
  }

  @override
  Widget build(BuildContext context) {
    myLocale = Localizations.localeOf(context);

    return MaterialApp(
      home: Scaffold(
        body: ModalProgressHUD(
          inAsyncCall: showSpinner,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [
                  Theme.of(context).accentColor,
                  Theme.of(context).primaryColor,
                ],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
                  child: Form(
                    key: _formKey,
                    child: TextFormField(
                      onChanged: (val) {
                        setState(() {
                          username = val;
                        });
                      },
                      decoration: InputDecoration(
                        hintText: 'username',
                        border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(10.0),
                          ),
                        ),
                      ),
                      validator: (val) {
                        return checkString(val) ? null : "username too short";
                      },
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
                  child: TextFormField(
                    onChanged: (val) {
                      setState(() {
                        password = val;
                      });
                    },
                    decoration: InputDecoration(
                      hintText: 'password',
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ),
                OutlineButton(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 60.0,vertical: 12),
                    child: Text(
                      'Login',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  color: Colors.white54,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  onPressed: () async {
                    final form = _formKey.currentState;

                    // check if username field is too short or empty then don't do anything.
                    if (!form.validate()) {
                      return;
                    }
                    form.save();

                    // if username is accepted then show a spinner on the screen until the application
                    //
                    setState(() {
                      showSpinner = true;
                    });
                    Response userResponse = await getTokenforUser(isLoggedUser: true);
                    if (userResponse != null && userResponse.statusCode == HttpStatus.ok)
                      enterStoreInfoPage(response: userResponse);
                    else
                      buildErrorDialog(response: userResponse);
                    setState(() {
                      showSpinner = false;
                    });
                  },
                  borderSide: BorderSide(color: Colors.black, width: 0.5),
                ),
                TextButton(
                  child: Text(
                    'Login anonymous?',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.black,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  onPressed: () async {
                    setState(() {
                      showSpinner = true;
                    });
                    Response userResponse = await getTokenforUser(isLoggedUser: false);
                    if (userResponse != null && userResponse.statusCode == HttpStatus.ok)
                      enterStoreInfoPage(response: userResponse);
                    else
                      buildErrorDialog(response: userResponse);
                    setState(() {
                      showSpinner = false;
                    });
                  }
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
