// To parse this JSON data, do
//
//     final token = tokenFromJson(jsonString);

import 'dart:convert';

List<Token> tokenFromJson(String str) => List<Token>.from(json.decode(str).map((x) => Token.fromJson(x)));

String tokenToJson(List<Token> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Token {
  Token({
    this.clientId,
    this.clientSecret,
    this.username,
    this.password,
    this.playerId,
    this.language,
    this.gmt,
    this.isFromNotification,
  });

  String clientId;
  String clientSecret;
  String username;
  String password;
  String playerId;
  String language;
  int gmt;
  bool isFromNotification;

  factory Token.fromJson(Map<String, dynamic> json) => Token(
    clientId: json["clientId"],
    clientSecret: json["clientSecret"],
    username: json["Username"] == null ? null : json["Username"],
    password: json["Password"] == null ? null : json["Password"],
    playerId: json["PlayerId"] ,
    language: json["Language"],
    gmt: json["GMT"],
    isFromNotification: json["IsFromNotification"],
  );

  Map<String, dynamic> toJson() => {
    "clientId": clientId,
    "clientSecret": clientSecret,
    "Username": username == null ? null : username,
    "Password": password == null ? null : password,
    "PlayerId": playerId,
    "Language": language,
    "GMT": gmt,
    "IsFromNotification": isFromNotification,
  };
}
