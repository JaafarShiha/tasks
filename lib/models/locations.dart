import 'package:flutter/material.dart';

// To parse this JSON data, do
//
//     final location = locationFromJson(jsonString);

import 'dart:convert';

List<Locations> locationFromJson(String str) => List<Locations>.from(json.decode(str).map((x) => Locations.fromJson(x)));

String locationToJson(List<Locations> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Locations {
  Locations({
    this.id,
    this.typeId,
    this.parentId,
    this.name,
    this.gps,
    this.borderNodes,
    this.isActive,
    this.children,
  });

  int id;
  int typeId;
  int parentId;
  String name;
  dynamic gps;
  dynamic borderNodes;
  bool isActive;
  List<Locations> children;

  factory Locations.fromJson(Map<String, dynamic> json) => Locations(
    id: json["id"],
    typeId: json["typeId"],
    parentId: json["parentId"] == null ? null : json["parentId"],
    name: json["name"],
    gps: json["gps"],
    borderNodes: json["borderNodes"],
    isActive: json["isActive"],
    children: List<Locations>.from(json["children"].map((x) => Locations.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "typeId": typeId,
    "parentId": parentId == null ? null : parentId,
    "name": name,
    "gps": gps,
    "borderNodes": borderNodes,
    "isActive": isActive,
    "children": List<dynamic>.from(children.map((x) => x.toJson())),
  };
}
