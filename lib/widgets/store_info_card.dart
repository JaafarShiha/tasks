import 'package:flutter/material.dart';
import 'package:tasks/widgets/dialog.dart';

// ignore: must_be_immutable
class StoreInfoCard extends StatefulWidget {
  final String image;
  final String title;
  final String description;
  final String phoneNumber;

  // isSelected variable determines if the current card will be expanded ( and showing more info) or not
  // if true then the card will be expanded ( and show more info, like : description and phone number)
  // if false then the card will show only the image and name of the store.
  bool isSelected;

  StoreInfoCard(
      {this.image,
      this.title,
      this.description,
      this.phoneNumber,
      this.isSelected});

  @override
  _StoreInfoCardState createState() => _StoreInfoCardState();
}

class _StoreInfoCardState extends State<StoreInfoCard> {
  buildBody() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        children: [
          ListTile(
            leading: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) =>
                        imageDialog(context, widget.image));
              },
              child: CircleAvatar(
                radius: 40.0,
                backgroundImage:
                    widget.image == null ? null : NetworkImage(widget.image),
                backgroundColor: Colors.teal[100],
              ),
            ),
            title: Text(
              widget.title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            trailing: IconButton(
              icon: Icon(
                widget.isSelected
                    ? Icons.keyboard_arrow_up
                    : Icons.keyboard_arrow_down,
              ),
              tooltip: widget.isSelected ? 'Show less info' : 'Show more info',
              onPressed: () {
                setState(() {
                  widget.isSelected = !widget.isSelected;
                });
              },
            ),
          ),
          // showMoreInfo is a widget returns the expanded part.
          widget.isSelected ? showMoreInfo() : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Divider(
              color: Colors.teal[100],
              thickness: 4,
            ),
          ),
        ],
      ),
    );
    //
  }

  // showMoreInfo is a widget returns the expanded part.
  showMoreInfo() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Description:\n ',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
                TextSpan(
                  text: widget.description,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
            child: Divider(
              thickness: 1.0,
              color: Colors.black,
            ),
          ),
          Center(
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'phone number: ',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                    ),
                  ),
                  TextSpan(
                    text: widget.phoneNumber,
                    style: TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildBody();
  }
}
