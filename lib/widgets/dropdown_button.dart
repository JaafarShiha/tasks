import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownButtonWidget extends StatefulWidget {
  final List<String> namesList;
  final String selectedValue;
  final Function onChanged;
  final String hint;

  DropDownButtonWidget(
      {this.namesList, this.selectedValue, this.onChanged, this.hint});

  @override
  _DropDownButtonWidgetState createState() => _DropDownButtonWidgetState();
}

class _DropDownButtonWidgetState extends State<DropDownButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(7.0),
      child: DropdownButton(
        items: widget.namesList.map((String name) {
          return DropdownMenuItem(
            child: Text(
              name,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            value: name,
          );
        }).toList(),
        value: widget.selectedValue,
        onChanged: widget.onChanged,
        hint: Text(
          widget.hint,
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        // style: ,
      ),
    );
  }
}
