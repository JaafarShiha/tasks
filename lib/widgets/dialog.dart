import 'package:flutter/material.dart';


// This Dialog will show error dialog when anything wrong happens in calling the API or wrong data.
Dialog errorDialog(context, String message) {
  return Dialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
    //this right here
    child: Container(
      height: 300.0,
      width: 300.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Text(
              message,
              style: TextStyle(color: Colors.red),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 50.0)),
          FlatButton(
            onPressed: () {
              // Go back to the previous page.
              Navigator.of(context).pop();
            },
            child: Text(
              'Got It!',
              style: TextStyle(color: Colors.purple, fontSize: 18.0),
            ),
          )
        ],
      ),
    ),
  );
}

// This Dialog will appear when the client clicks on circle avatar image and then the image will be shown in a dialog
Dialog imageDialog(context, String image) {
  return Dialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
    //this right here
    child: Container(
      height: 300.0,
      width: 300.0,
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        image: image == null ? null : DecorationImage(
          image:  NetworkImage(image) ,
          fit: BoxFit.cover,
        ),
      ),
    ),
  );
}
