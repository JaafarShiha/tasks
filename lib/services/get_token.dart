import 'dart:convert';

import 'package:http/http.dart' as http;
  String url =
      'https://storeak-identity-service-beta.azurewebsites.net/api/v1/token';

  Map<String, String> headers = {"Content-type": "application/json"};


  Future<http.Response> getAnonymousToken({requestBody}) async{
      // make POST request
      http.Response response = await http.post(url, headers: headers, body: jsonEncode(requestBody));
    return response;
  }

   Future<http.Response> getToken({requestBody}) async{
      // make POST request
      http.Response response = await http.post(url, headers: headers, body: jsonEncode(requestBody));
    return response;
  }
