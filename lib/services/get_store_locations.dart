import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tasks/models/locations.dart';

// This method return all Locations from our API
Future<List<dynamic>> getStoreLocation(
    String url, String tokenType, String accessToken) async {
  Map<String, String> headers = {
    "Content-type": "application/json",
    'Accept': 'application/json',
    "authorization": "Bearer $tokenType"
  };
  http.Response response = await http.get(url, headers: headers);

  List<dynamic> body = jsonDecode(response.body);

  return body; //jsonDecode(response.body);
}

// This method returns all countries names.
List<String> getCountriesNames(List data) {
  return data
      .map((map) => Locations.fromJson(map))
      .map((item) => item.name)
      .toList();
}

// This method returns all cities data for a specific country ( we pass to this method two arguments:
// FIRST: (LIST) countriesData: data we want to search in it.
// SECOND: (STRING) countryName: the name of the country we want to get all its cities data. )

// In another meaning: This method returns children attribute for a specific country in our API.
getCitiesData(List countriesData, String countryName) {
  // So here we get the cities data for specific country as a String.
  String cities = countriesData
      .map((map) => Locations.fromJson(map))
      .where((item) => item.name == countryName)
      .map((e) => jsonEncode(e.children))
      .first;

  // Here we cast the previous String to a List.
  List citiesData = jsonDecode(cities) as List;

  return citiesData;
}

// In the following method we return all cities names
// they are in a specific country  ( we pass to this method two arguments:
// FIRST: (LIST) countriesData: data we want to search in it.
// SECOND: (STRING) countryName: the name of the country we want to get all its cities names. )

// -------- THIS METHOD DEPENDS ON THE PREVIOUS ONE TO GET CITIES DATA AND AFTER THAT RETURN CITIES NAME. --------
List<String> getCitiesNames(List countriesData, String countryName) {
  List citiesData = getCitiesData(countriesData, countryName);

  return citiesData
      .map((map) => Locations.fromJson(map))
      .map((item) => item.name)
      .toList();
}

// This method return the all districts names for a specific country ( we pass to this method two arguments:
// FIRST: (LIST) cityData: data we want to search in it.
// SECOND: (STRING) cityName: the name of the city we want to get all its districts names. )
List<String> getDistrictsNames(List cityData, String cityName) {
  // returning districts data (children attribute of a city).
  String districts = cityData
      .map((map) => Locations.fromJson(map))
      .where((item) => item.name == cityName)
      .map((e) => jsonEncode(e.children))
      .first;

  List districtsData = jsonDecode(districts) as List;

  return districtsData
      .map((map) => Locations.fromJson(map))
      .map((item) => item.name)
      .toList();
}
