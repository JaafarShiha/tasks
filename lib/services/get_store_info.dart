import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future getStoreInfo(String url, String accessToken, String tokenType) async {
  Map<String, String> headers = {
    "Content-type": "application/json",
    'Accept': 'application/json',
    "authorization": "Bearer $accessToken",
  };
  http.Response response = await http.get(url, headers: headers);

  return jsonDecode(response.body);
}
